# Donverse.net

私達は、Discordに拠点を置く日本出身の開発コミュニティチームです。<br>
私達は、以下のプロジェクトを作成しています。

> - YuricaProject: [Link here](https://gitlab.com/donverse-dotnet/yurica-project)

# Links
> - Website: [Not deployed](https://www.donverse.net/)
> - Discord: [Join to our discord server!](https://discord.com/invite/FnMrTKPZAz)
> - X (Twitter): [See here](https://x.com/donverse_net)
> - Bluesky: [See here](https://bsky.app/profile/donverse-dotnet.bsky.social)
> - YouTube: [See here](https://www.youtube.com/@donverse-dotnet)
